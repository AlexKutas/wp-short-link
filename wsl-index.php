<?php
/*
 * Plugin Name: WP Short Link
 * Description: WP Short Link
 * Version: 1.0.0
 * Author: Alex Kutas
 * Author URI: https://t.me/alexkutas
 * Text Domain: wsl
 * Domain Path: /language
 */

add_action( 'plugins_loaded', function(){
	load_plugin_textdomain( 'wsl', false, dirname( plugin_basename(__FILE__) ) . '/language' );
} );

/**
 * Generate ID short link
 */
function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


/*
 * Add custom type page Short Link
 */
add_action('init', 'short_link_init');
function short_link_init(){
	register_post_type('short-link', array(
		'labels'             => array(
			'name'               => __('Short Link','wsl'),
			'singular_name'      => __('Short Link','wsl'),
			'add_new'            => __('Add new','wsl'),
			'add_new_item'       => __('Add new short link','wsl'),
			'edit_item'          => __('Edit short link','wsl'),
			'new_item'           => __('New short link','wsl'),
			'search_items'       => __('Shearch short link','wsl'),
			'not_found'          => __('No short link cure was found','wsl'),
			'not_found_in_trash' => __('No short links found in the cart','wsl'),
			'menu_name'          => __('Short Links','wsl')

		),
		'public'             => true,
		'publicly_queryable' => false,
		'exclude_from_search'=> true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_admin_bar'	 => false,
		'query_var'          => false,
		'rewrite'            => false,
		'capability_type'    => 'page',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 5,
		'menu_icon'			 => 'dashicons-admin-links',
		'supports'           => array('title'),
		'query_var'			 => false
	) );
}

/*
 * Add meta box select post for short link
 */
add_action('add_meta_boxes', 'wsl_add_short_link_post_box');
function wsl_add_short_link_post_box(){
	$screens = array( 'short-link' );
	add_meta_box( 'wsl_short_link_post', __('Short Link', 'wsl'), 'wsl_short_link_post_box', $screens );
}

/*
 * HTML-code meta box select post for short link
 */
function wsl_short_link_post_box( $post, $meta ){
	$screens = $meta['args'];

	/*
	 * We use nonce for verification
	 */
	wp_nonce_field( plugin_basename(__FILE__), 'wp-short-link' );

	/*
	 * Field value
	 */
	$wsl_post_link = get_post_meta( $post->ID, 'wsl-post-link', 1 );
	$wsl_short_link_id = get_post_meta ( $post->ID, 'wsl-short-link-id', 1 ) ?: generateRandomString();
	$wsl_short_link = get_home_url('null', '/') . $wsl_short_link_id;

	global $post;
	$all_post = get_posts(
		array(
			'posts_per_page' => -1,
			'post_type' 	 => 'post',
			'meta_query' => array(
				'book_color' => array(
					'key'     => 'wsl-short-link-enabled',
					'value'   => '1',
					'compare' => 'NOT EXISTS'
				)
			)
		)
	);

	if (get_post_meta ( $post->ID, 'wsl-short-link-id', 1 )) {
		echo '<div><label for="wsl-short-url-link"><b>' . __('Post','wsl') . ': </b> '. get_the_title(url_to_postid($wsl_post_link)) .'</label> ';
		echo '<input type="text" id="wsl-post-link" name="wsl-post-link" value="'. $wsl_post_link .'" hidden/><div>';

	} else {
		echo '<div><label for="wsl-short-url-link"><b>' . __('Post','wsl') . ': </b></label> ';
		echo '<select type="text" id="wsl-post-link" name="wsl-post-link">';
		foreach( $all_post as $post ){
			setup_postdata($post);
			$selected = '';
			if ($wsl_post_link == get_permalink()) {
				$selected = 'selected';
			}
			?>
			<option value="<?php echo get_permalink(); ?>" <?php echo $selected; ?>><?php the_title(); ?></option>
			<?php
		}
		echo '<select></div>';
	}
	wp_reset_postdata();
	echo '<div><br><b>'. __('Short Link','wsl') .': </b><a href="'.$wsl_short_link.'">'.$wsl_short_link.'</a></div>';
	echo '<input type="text" id="wsl-short-link-id" name="wsl-short-link-id" value="'. $wsl_short_link_id .'" hidden/><div>';

}



/*
 * Save data when short link population
 */
add_action( 'save_post', 'wsl_save_postdata' );
function wsl_save_postdata( $post_id ) {
	/*
	 * Make sure the field is set
	 */
	if ( ! isset( $_POST['wsl-short-link-id'] ) )
		return;

	/*
	 * Check nonce of our page, because save_post can be called from another place
	 */
	if ( "short-link" != $_POST['post_type'] )
		return;

	/*
	 * If it's autosave don't do anything
	 */
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return;

	/*
	 * Check user rights
	 */
	if( ! current_user_can( 'edit_post', $post_id ) )
		return;

	/*
	 * All OK. Now, you need to find and save the data
	 * We clear the value of the input field
	 */
	$wsl_short_link_id = $_POST['wsl-short-link-id'];
	$wsl_post_link = $_POST['wsl-post-link'];

	/*
	 * We update the data in the database
	 */
	update_post_meta( $post_id, 'wsl-short-link-id', $wsl_short_link_id );
	update_post_meta( $post_id, 'wsl-post-link', $wsl_post_link );

	/*
	 * Add a label that the post has a short link
	 */
	update_post_meta( url_to_postid($wsl_post_link), 'wsl-short-link-enabled', true );

	remove_action( 'save_post', 'wsl_save_postdata' );

	/*
	 * If there is no title, take it from the short link
	 */
	$post_title = $_POST['post_title'];
	if ( empty($_POST['post_title']) ) {
		$post_title = get_the_title(url_to_postid($wsl_post_link));
	}

	wp_update_post( array('ID' => $post_id, 'post_title' => $post_title, 'post_name'=> $wsl_short_link_id));
	remove_action( 'save_post', 'wsl_save_postdata' );

}


/*
 * Hook clearing a label of a short link record when deleting a short link
 */
add_action( 'before_delete_post', 'wsl_delete_postdata' );
function wsl_delete_postdata( $postid ){

	/*
	 * Check if our record type is deleted
	 */
	$post = get_post( $postid );

	/*
	 * If not, get out
	 */
	if( ! $post || $post->post_type !== 'short-link' )
		return;

	$wsl_post_link = get_post_meta( $postid, 'wsl-post-link', 1 );
	delete_post_meta( url_to_postid($wsl_post_link), 'wsl-short-link-enabled', false );

}

/*
 * Add a hook to handle short links
 */
add_action( 'wp_loaded', 'wsl_init' );
function wsl_init() {

	/*
	 * Get short link id
	 */
	$wsl_get_link = substr(strrchr($_SERVER['REQUEST_URI'], "/"), 1);

	/*
	 * Processing short link id
	 */
	if ($get_post_short_link = get_page_by_path($wsl_get_link, OBJECT, 'short-link')) {

		/*
		 * Get the link to redirect the user to
		 */
		$wsl_link = get_post_meta( $get_post_short_link->ID, 'wsl-post-link', 1 );

		/*
		 * Redirect user if there is a link
		 */
		if ($wsl_link) {
			header("Location: $wsl_link");
			exit;
		}

	}
}

/*
 * Сreate a new column on the admin page for short links
 */
add_filter( 'manage_'.'short-link'.'_posts_columns', 'add_short_link_column', 4 );
function add_short_link_column( $columns ){
	$num = 2;
	$new_columns = array(
		'short-link' => __('Short Link', 'wsl'),
	);

	return array_slice( $columns, 0, $num ) + $new_columns + array_slice( $columns, $num );
}

/*
 *  Fill the column with data
 */
add_action('manage_'.'short-link'.'_posts_custom_column', 'short_link_column', 5, 2 );
function short_link_column( $colname, $post_id ){
	if( $colname === 'short-link' ){
		$wsl_short_link_id = get_post_meta ( $post_id, 'wsl-short-link-id', 1 );
		$wsl_short_link = get_home_url('null', '/') . $wsl_short_link_id;
		echo $wsl_short_link;
	}
}

?>
